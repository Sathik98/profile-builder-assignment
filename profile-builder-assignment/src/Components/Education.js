import React, { useState } from "react";
import './Education.css';

function Education(props) {

    const [inputs, setInputs] = useState({});
    // const [fristName, setFristName] = useState("");

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }


    const handleSubmit = (e) => {
        e.preventDefault();


        const set = localStorage.setItem("educationDetails", JSON.stringify(inputs));
        // const user = JSON.parse(localStorage.getItem("educationDetails"));
        // const set= localStorage.setItem("experienceDetails", "user");

        props.save((pre)=>pre+1)

        console.log(set)


        console.log(inputs);
    }

    return (
        <>

            {/* add education */}

            <div className="center">

                <form onSubmit={handleSubmit} style={{ height: "700px" }}>

                    <div className="contentthree">

                        <h2 className="aedu">Add Education</h2>

                        <label>School:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="school"
                                placeholder="Enter school name"
                                required="required"
                                value={inputs.school || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br /><br />

                        <label>Degree:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="degree"
                                placeholder="enter degree"
                                required="required"
                                value={inputs.degree || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br /><br />

                        <label>Field of Study:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="field"
                                placeholder="enter study"
                                required="required"
                                value={inputs.field || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br /><br />

                        <label>Start Date: <span className="req">*</span></label>

                        <br />

                        <select className="startmonth" name="sdate" style={{ width: "325px" }}>
                            <option value="0">Month</option>
                            <option value="1">january</option>
                            <option value="2">febuary</option>
                            <option value="3">march</option>
                            <option value="4">april</option>
                            <option value="5">may</option>
                            <option value="6">june</option>
                            <option value="7">july</option>
                            <option value="8">august</option>
                            <option value="9">september</option>
                            <option value="10">october</option>
                            <option value="11">november</option>
                            <option value="12">december</option>
                        </select>

                        <select className="startyear" style={{ width: "325px" }}>
                            <option value="0">Year</option>
                            <option value="1">2000</option>
                            <option value="2">2001</option>
                            <option value="3">2002</option>
                            <option value="4">2003</option>
                            <option value="5">2004</option>
                            <option value="6">2005</option>
                            <option value="7">2006</option>
                            <option value="8">2007</option>
                            <option value="9">2008</option>
                            <option value="10">2009</option>
                            <option value="11">2010</option>
                            <option value="12">2011</option>
                            <option value="13">2012</option>
                            <option value="14">2013</option>
                            <option value="15">2014</option>
                            <option value="16">2015</option>
                            <option value="17">2016</option>
                            <option value="18">2017</option>
                            <option value="19">2018</option>
                            <option value="20">2019</option>
                            <option value="21">2020</option>
                            <option value="22">2021</option>
                        </select>

                        <br /><br />

                        <label>End Date: <span className="req">*</span></label>

                        <br />

                        <select className="endmonth" style={{ width: "325px" }}>
                            <option value="0">Month</option>
                            <option value="1">january</option>
                            <option value="2">febuary</option>
                            <option value="3">march</option>
                            <option value="4">april</option>
                            <option value="5">may</option>
                            <option value="6">june</option>
                            <option value="7">july</option>
                            <option value="8">august</option>
                            <option value="9">september</option>
                            <option value="10">october</option>
                            <option value="11">november</option>
                            <option value="12">december</option>
                        </select>

                        <select className="endyear" style={{ width: "325px" }}>
                            <option value="0">Year</option>
                            <option value="1">2000</option>
                            <option value="2">2001</option>
                            <option value="3">2002</option>
                            <option value="4">2003</option>
                            <option value="5">2004</option>
                            <option value="6">2005</option>
                            <option value="7">2006</option>
                            <option value="8">2007</option>
                            <option value="9">2008</option>
                            <option value="10">2009</option>
                            <option value="11">2010</option>
                            <option value="12">2011</option>
                            <option value="13">2012</option>
                            <option value="14">2013</option>
                            <option value="15">2014</option>
                            <option value="16">2015</option>
                            <option value="17">2016</option>
                            <option value="18">2017</option>
                            <option value="19">2018</option>
                            <option value="20">2019</option>
                            <option value="21">2020</option>
                            <option value="22">2021</option>
                        </select>

                        <br /><br />

                        <label>Grade:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="grade"
                                placeholder="enter Grade"
                                required="required"
                                value={inputs.grade || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br /><br />

                        <label>Activities:
                            <span className="req">*</span>
                            <br />
                            <textarea
                                className="des"
                                type="text"
                                name="description"
                                placeholder="Ex: Cricket, drawing, etc.,"
                                required="required"
                                value={inputs.description || ""}
                                onChange={handleChange}
                            ></textarea>
                        </label>
                    </div>

                    <div className="btn">
                        <button type="submit" className="btno">Save</button>
                    </div>
                </form>
            </div>
        </>


    )
}

export default Education;