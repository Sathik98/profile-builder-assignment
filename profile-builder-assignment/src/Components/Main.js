import React, { useState } from 'react';
import './Main.css';
import Education from './Education';
import Experience from './Experience';
import Template from './Template';




function Main() {
    const [inputs, setInputs] = useState({});
    const [setTempleteUser]=useState({});
    const [save,setsave]=useState(1)

    // const [fristName, setFristName] = useState("");

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }
    // const value={
    //     fristname:""
    // }

    const handleSubmit = (e) => {
        e.preventDefault();



        const set = localStorage.setItem("userDetails", JSON.stringify(inputs));
        const user = JSON.parse(localStorage.getItem("userDetails"));
        
        setTempleteUser(user);

        console.log(set)
        setsave((pre)=>pre+1);


        console.log(inputs);
    }


    return (
        <>

        <div className="center">
            <form onSubmit={handleSubmit} style={{ height: "680px" }}>
                <p className="ir"><span className="req">*</span> indicates required </p>

                <div className="contentone">
                    <label>first name:
                        <span className="req">*</span>

                        <br />

                        <input
                            type="text"
                            name="fname"
                            placeholder="Enter First Name"
                            required="required"
                            value={inputs.fname || ""}
                            onChange={handleChange}
                            style={{ width: "725px"}}
                        />
                    </label>

                    <br /><br />

                    <label>last name:
                        <span className="req">*</span>
                        <br />
                        <input
                            type="text"
                            name="lname"
                            placeholder="Enter last Name"
                            required="required"
                            value={inputs.lname || ""}
                            onChange={handleChange}
                            style={{ width: "725px" }}
                        />
                    </label>

                    <br /><br />

                    <label>Title:
                        <span className="req">*</span>
                        <br />
                        <input
                            type="text"
                            name="title"
                            placeholder="Enter Title"
                            required="required"
                            value={inputs.title || ""}
                            onChange={handleChange}
                            style={{ width: "725px" }}
                        />
                    </label>
                </div>

                <br /><br />

                <h2 className="od">Other Details</h2>

                <div className="contenttwo">
                    <label>phone:
                        <span className="req">*</span>
                        <br />
                        <input
                            type="number"
                            name="phone"
                            placeholder="Enter phone number"
                            required="required"
                            value={inputs.phone || ""}
                            onChange={handleChange}
                            style={{ width: "725px" }}
                        />
                    </label>

                    <br /> <br />

                    <label>Email:
                        <span className="req">*</span>
                        <br />
                        <input
                            type="text"
                            name="email"
                            placeholder="Enter Email ID"
                            required="required"
                            value={inputs.email || ""}
                            onChange={handleChange}
                            style={{ width: "725px" }}
                        />
                    </label>

                    <br /><br />

                    <label>Linkedin ID:
                        <span className="req">*</span>
                        <br />
                        <input
                            type="text"
                            name="linkedin"
                            placeholder="Enter linkedin ID"
                            required="required"
                            value={inputs.linkedin || ""}
                            onChange={handleChange}
                            style={{ width: "725px" }}
                        />
                    </label>

                    <br /><br />

                    <label>Location:
                        <span className="req">*</span>
                        <br />
                        <input
                            type="text"
                            name="location"
                            placeholder="Enter location"
                            required="required"
                            value={inputs.location || ""}
                            onChange={handleChange}
                            style={{ width: "725px" }}
                        />
                    </label>
                </div>

                <div className="btn">
                    <button type="submit" className="btno">Save</button>
                </div>


            </form>
        </div>

        <Experience save={setsave} />
      <Education save={setsave} />
      
      <Template profile={save}/>
        </>
    )
}


export default Main;
