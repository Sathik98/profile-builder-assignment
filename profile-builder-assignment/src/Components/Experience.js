import React, { useState } from "react";
import './Experience.css';

function Experience(props) {

    const [inputs, setInputs] = useState({});
    // const [fristName, setFristName] = useState("");

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }


    const handleSubmit = (e) => {
        e.preventDefault();


        const set = localStorage.setItem("experienceDetails", JSON.stringify(inputs));
        // const user = JSON.parse(localStorage.getItem("experienceDetails"));
        // const set= localStorage.setItem("experienceDetails", "user");
            props.save((pre)=>pre+1)

        console.log(set)


        console.log(inputs);
    }

    return (
        <>

            {/* add experience */}

            <div className="center">

                <form onSubmit={handleSubmit} style={{ height: "600px" }}>

                    <h2 className="ae">Add Experience</h2>

                    <div className="contentthree">

                        <label>Title:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="title"
                                placeholder="Ex: Front-End"
                                required="required"
                                value={inputs.title || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br />

                        <p>Employment Type: <span className="req">*</span></p>

                        <select className="dropdown" style={{ width: "725px" }}>
                            <option value="N/A">N/A</option>
                            <option value="1">full-time</option>
                            <option value="2">part-time</option>
                            <option value="3">internship</option>
                        </select>

                        <br />

                        <label>Company Name:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="company"
                                placeholder="Ex: Microsoft"
                                required="required"
                                value={inputs.company || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br /><br />
                        <label>Location:
                            <span className="req">*</span>
                            <br />
                            <input
                                type="text"
                                name="location"
                                placeholder="EX: Chennai, Tamil Nadu"
                                required="required"
                                value={inputs.location || ""}
                                onChange={handleChange}
                                style={{ width: "725px" }}
                            />
                        </label>

                        <br /><br />

                        <input
                            type="checkbox"
                            defaultChecked={true}
                        />
                        <label>i am currently working in this role</label>

                        <br /><br />

                        <label>Start Date: <span className="req">*</span></label>
                        <br />
                        <select className="startmonth" name="date" style={{ width: "325px" }}>
                            <option value="0">Month</option>
                            <option value="1">january</option>
                            <option value="2">febuary</option>
                            <option value="3">march</option>
                            <option value="4">april</option>
                            <option value="5">may</option>
                            <option value="6">june</option>
                            <option value="7">july</option>
                            <option value="8">august</option>
                            <option value="9">september</option>
                            <option value="10">october</option>
                            <option value="11">november</option>
                            <option value="12">december</option>
                        </select>

                        <select className="startyear" name="date" style={{ width: "325px" }}>
                            <option value="0">Year</option>
                            <option value="1">2000</option>
                            <option value="2">2001</option>
                            <option value="3">2002</option>
                            <option value="4">2003</option>
                            <option value="5">2004</option>
                            <option value="6">2005</option>
                            <option value="7">2006</option>
                            <option value="8">2007</option>
                            <option value="9">2008</option>
                            <option value="10">2009</option>
                            <option value="11">2010</option>
                            <option value="12">2011</option>
                            <option value="13">2012</option>
                            <option value="14">2013</option>
                            <option value="15">2014</option>
                            <option value="16">2015</option>
                            <option value="17">2016</option>
                            <option value="18">2017</option>
                            <option value="19">2018</option>
                            <option value="20">2019</option>
                            <option value="21">2020</option>
                            <option value="22">2021</option>
                        </select>

                        <br /><br />

                        <label>End Date: <span className="req">*</span></label>

                        <br />

                        <select className="endmonth" style={{ width: "325px" }}>
                            <option value="0">Month</option>
                            <option value="1">january</option>
                            <option value="2">febuary</option>
                            <option value="3">march</option>
                            <option value="4">april</option>
                            <option value="5">may</option>
                            <option value="6">june</option>
                            <option value="7">july</option>
                            <option value="8">august</option>
                            <option value="9">september</option>
                            <option value="10">october</option>
                            <option value="11">november</option>
                            <option value="12">december</option>
                        </select>

                        <select className="endyear" style={{ width: "325px" }}>
                            <option value="0">Year</option>
                            <option value="1">2000</option>
                            <option value="2">2001</option>
                            <option value="3">2002</option>
                            <option value="4">2003</option>
                            <option value="5">2004</option>
                            <option value="6">2005</option>
                            <option value="7">2006</option>
                            <option value="8">2007</option>
                            <option value="9">2008</option>
                            <option value="10">2009</option>
                            <option value="11">2010</option>
                            <option value="12">2011</option>
                            <option value="13">2012</option>
                            <option value="14">2013</option>
                            <option value="15">2014</option>
                            <option value="16">2015</option>
                            <option value="17">2016</option>
                            <option value="18">2017</option>
                            <option value="19">2018</option>
                            <option value="20">2019</option>
                            <option value="21">2020</option>
                            <option value="22">2021</option>
                        </select>
                    </div>

                    <div className="btn">
                        <button type="submit" className="btno">Save</button>
                    </div>
                    
                </form>
            </div>
        </>
    )
}

export default Experience;