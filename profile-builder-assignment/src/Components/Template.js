import React, { useState, useEffect } from 'react';
import './Template.css';
import img0 from '../Images/user.png';
import { BsLinkedin } from "react-icons/bs";
import { BsPhone } from "react-icons/bs";
import { BsEnvelope } from "react-icons/bs";
import { BsPinMapFill } from "react-icons/bs";


function Template(props) {
    const [user, setuser] = useState({});
    const [workEx, setWork] = useState({
        title: " "
    })
    const [educa, setEduca] = useState({
        title: " "
    })


    useEffect(() => {
        const users = JSON.parse(localStorage.getItem("userDetails"));
        const exp = JSON.parse(localStorage.getItem("experienceDetails"));
        const edu = JSON.parse(localStorage.getItem("educationDetails"));

        console.log(exp)
        // if(users!==" " || exp!==" "){
        //     setuser(users);
        //     setWork(exp)
        // }
        // if(!exp || users){
        //    return;
        // }
        if (exp) {
            setWork(exp)
        }
        if (users) {
            setuser(users);
        }
        if (edu) {
            setEduca(edu)
        }



    }, [props.profile])


    return (

        <>
            <br />

            {/* User Profile(Main) */}

            <div className="container">
                <div className="boxone"></div>
                <div className="text">
                    <h1 className="first-name">{user.fname || "name"}</h1>
                    <h1 className="last-name">{user.lname || "name"}</h1>
                    <h2 className="title">{user.title || "title"}</h2>

                    <p className="msg">A ReactJS developer is responsible for designing <br />
                        and implementing UI components for JavaScript-based web applications<br />
                        web applications and mobile applications with the use of<br />
                        open-source library infrastructure library infrastructure </p>
                </div>
                <div className="img">
                    <img className="imgedit" src={img0} alt="Profile" />
                </div>

                <div className="contact">
                    <h2 className="mail">{user.email || "email"}<BsEnvelope className="mi" /></h2>
                    <h2 className="phone">{user.phone || "phone"}<BsPhone className="pi" /></h2>
                    <h2 className="linked">{user.linkedin || "linkedin"}<BsLinkedin className="li" /></h2>
                    <h2 className="location">{user.location || "location"}<BsPinMapFill className="loi" /></h2>
                </div>

                <div className="line">
                    <hr className="lineedit"></hr>
                </div>

                {/* Experience */}

                <div className="experience">
                    <div className="boxtwo"></div>
                    <h1 className="workexp">Work Experience</h1>
                    <h1 className="designation">{workEx.title || "software"}</h1>
                    <h1 className="company">{workEx.company || "Google"}</h1>
                    <h2 className="date">{workEx.sdate || "sep 2021"}</h2>
                    <h2 className="place">{workEx.location || "madurai"}</h2>
                </div>

                {/* Education */}

                <div className="boxthree"></div>
                <div className="education">
                    <h1 className="edu">Education</h1>
                    <h3 className="school">{educa.school || "kln"}</h3>
                    <h3 className="degree">{educa.degree || "B.E"}</h3>
                    <h3 className="field">{educa.field || "ECE"}</h3>
                    <h3 className="startdate">Started: mar, 2016</h3>
                    <h3 className="enddate">Completed: sep, 2021</h3>
                    <h3 className="grade">{educa.grade || "B+"}</h3>
                    <h3 className="activities">{educa.description || "football"}</h3>
                </div>

                {/* skills */}

                <div>

                    <div className="boxfour"></div>

                    <h1 className="ski">Skills:</h1>

                    <p className="sp">suggestion skills based on your profile</p>

                    <div className="html">
                    <label>HTML5

                        <input
                            type="checkbox"
                            name="html"
                            className="box"
                        />

                        <span class="checkmark"></span>

                    </label>
                    </div>

                    <br/><br/>

                    <div className="css">
                    <label>CSS3

                        <input
                            type="checkbox"
                            name="css"
                            className="box"
                        />

                        <span class="checkmark"></span>

                    </label>
                    </div>

                    <div className="js">
                    <label>JavaScript

                        <input
                            type="checkbox"
                            name="js"
                            className="box"
                        />

                        <span class="checkmark"></span>

                    </label>
                    </div>

                    <br/><br/>

                    <div className="react">
                    <label>ReactJS

                        <input
                            type="checkbox"
                            name="react"
                            className="box"
                        />

                        <span class="checkmark"></span>

                    </label>
                    </div>

                    <div className="angular">
                    <label>AngularJS

                        <input
                            type="checkbox"
                            name="angular"
                            className="box"
                        />

                        <span class="checkmark"></span>

                    </label>
                    </div>

                    <br/><br/>

                    <div className="bootstrap">
                    <label>BOOTSTRAP

                        <input
                            type="checkbox"
                            name="bootstrap"
                            className="box"
                        />

                        <span class="checkmark"></span>

                    </label>
                    </div>






                </div>








            </div>

        </>
    )
}

export default Template;
